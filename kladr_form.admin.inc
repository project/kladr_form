<?php

/**
 * @file
 * Administrative page callbacks for the kladr_form module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 * @param $form_state
 */
function kladr_form_admin_settings_form($form_state) {
	$form = array();
	
	$form['kladr_form_token'] = array(
			'#type' => 'textfield',
			'#title' => t('Token for using the paid service'),
			'#description' => t('Leave this field blank to use the free service.'),
			'#required' => FALSE,
			'#default_value' => variable_get('kladr_form_token'),
			'#size' => 36,
			'#maxlength' => 36,
	);
	
	$form['kladr_form_element_type'] = array(
			'#type' => 'textfield',
			'#title' => t('Element type to control with KLADR'),
			'#description' => t('For example, if you are using this with Ubercart, you would enter <i>uc_address</i> here to use with all address fields.'),
			'#required' => FALSE,
			'#default_value' => variable_get('kladr_form_element_type'),
			'#size' => 12,
			'#maxlength' => 36,
	);
	
	$form['ids'] = array(
			'#type' => 'fieldset',
			'#title' => t('Form field endings'),
			'#collapsible' => TRUE,
			'#collapsed' => FALSE
	);
	$form['ids']['kladr_form_street_field'] = array(
			'#type' => 'textfield',
			'#title' => t('Form field ending for street name'),
			'#description' => t('For example, if you are using this with Ubercart, you would enter <i>street1</i> here, since all uc_address street fields are formatted as something-street1.'),
			'#required' => TRUE,
			'#default_value' => variable_get('kladr_form_street_field'),
			'#size' => 12,
			'#maxlength' => 36,
	);
	$form['ids']['kladr_form_city_field'] = array(
			'#type' => 'textfield',
			'#title' => t('Form field ending for city name'),
			'#description' => t('For example, if you are using this with Ubercart, you would enter <i>city</i> here.'),
			'#required' => TRUE,
			'#default_value' => variable_get('kladr_form_city_field'),
			'#size' => 12,
			'#maxlength' => 36,
	);
	
	$form['ids']['kladr_form_zone_field'] = array(
			'#type' => 'textfield',
			'#title' => t('Form field ending for state/province/zone'),
			'#description' => t('For example, if you are using this with Ubercart, you would enter <i>zone</i> here. Setting this allows autocomplete from the KLADR.'),
			'#required' => FALSE,
			'#default_value' => variable_get('kladr_form_zone_field'),
			'#size' => 12,
			'#maxlength' => 36,
	);
	$form['ids']['kladr_form_country_field'] = array(
			'#type' => 'textfield',
			'#title' => t('Form field ending for country'),
			'#description' => t('For example, if you are using this with Ubercart, you would enter <i>country</i> here. Setting this allows the use of hooks for extending KLADR.'),
			'#required' => TRUE,
			'#default_value' => variable_get('kladr_form_country_field'),
			'#size' => 12,
			'#maxlength' => 36,
	);
	
	return system_settings_form($form);
}

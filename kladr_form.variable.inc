<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 *
 * @param $options
 * @return array of variables for kladr_form
 */
function kladr_form_variable_info($options) {
	$variables = array();
	
	$variables['kladr_form_token'] = array(
			'type' => 'string',
			'title' => t('Token for using the paid service', array(), $options),
			'description' => t('Leave this field blank to use the free service.', array(), $options),
			'required' => FALSE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	$variables['kladr_form_element_type'] = array(
			'type' => 'string',
			'title' => t('Element type to control with KLADR', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>uc_address</i> here to use with all address fields.', array(), $options),
			'required' => FALSE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	$variables['kladr_form_street_field'] = array(
			'type' => 'string',
			'title' => t('Form field ending for street name', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>street1</i> here, since all uc_address street fields are formatted as something-street1.', array(), $options),
			'required' => TRUE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	$variables['kladr_form_city_field'] = array(
			'type' => 'string',
			'title' => t('Form field ending for city name', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>city</i> here.', array(), $options),
			'required' => TRUE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	$variables['kladr_form_zone_field'] = array(
			'type' => 'string',
			'title' => t('Form field ending for state/province/zone', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>zone</i> here. Setting this allows autocomplete from the KLADR.', array(), $options),
			'required' => FALSE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	$variables['kladr_form_zip_field'] = array(
			'type' => 'string',
			'title' => t('Form field ending for postal code', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>postal_code</i> here. Setting this allows autocomplete of postal code from the KLADR.', array(), $options),
			'required' => FALSE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);	
	$variables['kladr_form_country_field'] = array(
			'type' => 'string',
			'title' => t('Form field ending for country', array(), $options),
			'description' => t('For example, if you are using this with Ubercart, you would enter <i>country</i> here. Setting this allows the use of hooks for extending KLADR.', array(), $options),
			'required' => TRUE,
			'validate callback' => 'kladr_form_validate_var',
			'group' => 'kladr_form',
	);
	
	return $variables;
}

/**
 * Implements hook_variable_group_info().
 *
 * @return array of group information for the variables
 */
function kladr_form_variable_group_info() {
	$groups = array();
	$groups['kladr_form'] = array(
			'title' => 'KLADR form',
			'description' => t('Access to the KLADR API.'),
			'access' => 'administer kladr_form',
			'path' => array('admin/settings/kladr_form'),
	);
	
	return $groups;
}


/**
 * Callback to validate vars
 *
 * @param array $variable to be validated
 */
function kladr_form_validate_var($variable) {
	if ($variable['value'] != trim($variable['value'])) {
		return t('Please remove leading and trailing white space from the value.');
	}
}
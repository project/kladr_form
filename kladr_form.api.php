<?php

/**
 * @file
 * Hooks provided by the KLADR form module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * A callback to use in kladr_form_autocomplete_city to extend for other countries.
 * 
 * Some systems extend KLADR and generate their own codes for places in other countries.
 * This hook will be called at the beginning of kladr_form_autocomplete_city to handle
 * such cases. The return value determines what kladr_form_autocomplete_city does further.
 * 
 * @param string the code for selected $country from the user module
 * @param string $str to search for city
 * @return array empty if defaults to the KLADR module; i.e., Russian Federation
 * otherwise array ('module key' => array of results; which empty if none, else city => city
 */
function hook_kladr_form_city_search($country, $str) {
	$results = array();
	
	if ($country == $some_value) {
		$results['my module'] = $matches;
	}
	
	return $results;
}

/**
 * A callback to use in kladr_form_autocomplete_street to extend for other countries.
 *
 * Some systems extend KLADR and generate their own codes for places in other countries.
 * This hook will be called at the beginning of kladr_form_autocomplete_street to handle
 * such cases. The return value determines what kladr_form_autocomplete_street does further.
 *
 * @param string the code for selected $country from the user module
 * @param string of the $city
 * @param string $str to search for street
 * @return array empty if defaults to the KLADR module; i.e., Russian Federation
 * otherwise array ('module key' => array of results; which empty if none, else city => city
 */
function hook_kladr_form_street_search($country, $city, $str) {
	$results = array();
	
	if ($country == $some_value) {
		$results['my module'] = $matches;
	}
	
	return $results;
}

/**
 * @} End of "addtogroup hooks".
 */


Module: KLADR API
Author: Jennifer Trelewicz <http://drupal.org/user/1601538>


Description
===========
Provides functionality for integration with the KLADR API for other modules

Requirements
============

Installation
============
Copy the 'kladr_form' module directory in to your Drupal sites/all/modules directory as usual.

Usage
=====
This is not a standalone module, there is no direct user interface.
The module provides callbacks for autocomplete in forms, as well as a string-to-KLADR 
function for retrieving autocompleted data.

If you need KLADR from text addresses (e.g. saved in Ubercart config earlier), it is
highly recommended to reenter the values after enabling this module to ensure that the
KLADR search will work on the string.